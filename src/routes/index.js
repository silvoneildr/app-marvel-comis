import React, { Suspense, lazy } from 'react';

import {
  Switch,
  Route,
  BrowserRouter as Router
} from 'react-router-dom';

const Characters = lazy(() => import('../pages/Characters'));
const CharacterDetails = lazy(() => import('../pages/CharacterDetails'));

export default function routes() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Router>
        <Switch>
          <Route path="/" component={Characters} exact />
          <Route path="/characters" component={Characters} />
          <Route path="/character/:id" component={CharacterDetails} />
        </Switch>
      </Router>
    </Suspense>
  );
}
