import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';
import { Container, NameCharacter } from './styles';
import defaultImage from '../../assets/images/not_avaliable.jpg';

function _Card({ name, imagePath }) {
  const [image, setImage] = useState(imagePath)
  function onError() {
    setImage(defaultImage)
  }
  return (
    <Container>
      <img
        src={image}
        data-testid="imageComp"
        alt="imageCharacterCard"
        onError={onError}
      />
      <NameCharacter>
        <span>{name}</span>
      </NameCharacter>
    </Container>
  );
}

_Card.propTypes = {
  name: PropTypes.string.isRequired,
  imagePath: PropTypes.string.isRequired,
}

export default memo(_Card);
