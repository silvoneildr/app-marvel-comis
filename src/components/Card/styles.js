import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #171515;
  color: white;
  cursor: pointer;
  span {
    text-align: center;
    font-weight: bold;
    font-size: 0.8rem;
  }
  &:hover {
    background-color: #EC1D24;
    text-decoration: none;
    transition: transform 0.2s;
    transform: scale(1.03);
    span {
      text-align: center;
      font-weight: bold;
      font-size: 0.9rem;
    }
  }
  img {
    width: 100%;
    height: 100%;
    opacity: 0.99;
    transition: opacity 0.5s ease-out;
    -webkit-transition: opacity 1.5s ease-out;
    -moz-transition: opacity 1.5s ease-out;
    -ms-transition: opacity 1.5s ease-out;
    -o-transition: opacity 1.5s ease-out;
  }
`;

export const NameCharacter = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 5rem;
  padding: 1rem;
  text-transform: uppercase;
`;
