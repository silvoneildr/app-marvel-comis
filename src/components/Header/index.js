import React from 'react';
import LogoMarvel from '../../assets/images/LogoMarvel';
import { Container, ContainerLogo } from './styles';

function Header() {
  return (
    <Container>
      <ContainerLogo>
        <LogoMarvel />
      </ContainerLogo>
    </Container>
  );
}

export default Header;