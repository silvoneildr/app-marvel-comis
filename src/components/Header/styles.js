import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  display: flex;
  margin: 0;
  padding: 0;
  align-items: center;
  justify-content: center;
  color: white;
  background-color: #171515;
`;

export const ContainerLogo = styled.div`
  display: flex;
  margin: 0 1rem;
`;
