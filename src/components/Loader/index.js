import React from 'react';

import { Container, Spinner } from './styles';

export default function Loader() {
  return (
    <Container>
      <Spinner />
      {/* <span>Carregando dados</span> */}
    </Container>
  );
}
