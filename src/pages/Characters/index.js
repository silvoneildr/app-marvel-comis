import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  fetchCharactersAction,
  searchByInitials,
} from '../../redux/characters/actions';
import Header from '../../components/Header';
import Card from '../../components/Card';
import IconSearch from '../../assets/images/icon-search.svg';
import {
  Container,
  ContentCharacters,
  ContainerSearch,
  ContainerInput,
  Input,
  CardLink,
} from './styles';

import Loader from '../../components/Loader'

function Characters() {
  const dispatch = useDispatch();
  const charactersList = useSelector(({ characters }) => ({
    ...characters,
    data: characters.data,
  }));

  useEffect(() => {
    function fetchData() {
      const params = charactersList.searchByInitials
        ? charactersList.searchByInitials
        : ''
      try {
       dispatch(fetchCharactersAction(params));
      } catch (error) {
        window.console.log(error)
      }
    }
    fetchData();
  }, [dispatch, charactersList.searchByInitials]);

  function searchCharacter(event) {
    if (event.key === 'Enter' || event.key === 13) {
      try {
        dispatch(fetchCharactersAction(event.target.value));
        dispatch(searchByInitials(event.target.value));
      } catch (error) {
        window.console.log(error)
      }
    }
  }

  return (
    <>
      <Header />
      <Container>
        <ContainerSearch>
          <ContainerInput>
            <img src={IconSearch} alt="IconSearch" width={15} />
            <Input
              id="inputSearchCharacter"
              data-testid="inputSearch"
              type="text"
              defaultValue={charactersList.searchByInitials}
              placeholder="Informe as iniciais do personagem e pressione enter"
              onKeyPress={searchCharacter}
            />
          </ContainerInput>
        </ContainerSearch>
        {charactersList.isLoading
          ? <Loader />
          : <ContentCharacters>
            {charactersList
              && charactersList.data
              && charactersList.data.length > 0
              && charactersList.data.map(item => (
              <CardLink
                key={item.id}
                to={{
                  pathname: `/character/${item.id}`,
                  state: { characterDetailsPage: true },
                }}
                >
                  <Card
                    key={item.id}
                    name={item.name}
                    imagePath={`${item.thumbnail.path}/portrait_incredible.${item.thumbnail.extension}`}
                  />
                </CardLink>
              ))}
          </ContentCharacters>}
      </Container>
    </>
  );
}

export default Characters;
