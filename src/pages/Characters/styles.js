import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 0 1rem;
`;

export const ContentCharacters = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, 11rem);
  grid-gap: 1rem;
  justify-content: space-between;
  width: 75vw;
`;

export const ContainerSearch =  styled.div`
  display: flex;
  padding: 3rem 0;
  justify-content: start;
  width: 75vw;
  img {
    margin: 0;
  }
`;

export const ContainerInput =  styled.div`
  display: flex;
  border: 0;
  border-bottom: 2px solid #151515;
  width: 100%;
`;

export const Input = styled.input`
  border: none;
  width: 100%;
  padding: 0.5rem;
  border-radius: 0;
  letter-spacing: 1px;
  text-transform: uppercase;
  background-color: transparent;
  :-webkit-autofill {
    transition: all 5000s ease-in-out 0s;
  }
`;

export const Button = styled.button`
  cursor: pointer;
  width: 45px;
`;

export const CardLink = styled(Link)`
  margin: 0;
  padding: 0;
  text-decoration: none;
`;
