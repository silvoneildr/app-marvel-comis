import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  width: 30rem;
`;

export const FormGroup = styled.div`
  color: #707070;
  display: block;
  width: 27rem;
  margin: 2rem auto;
`;

export const Label = styled.label`
  margin-bottom: 0.5em;
  color: #707070;
  display: block;
  font-size: 0.9rem;
`;

export const ContainerInput = styled.div`
  display: flex;
`

export const Input = styled.input`
  padding: 0.5em;
  color: #707070;
  background: #ccc;
  border: none;
  border-radius: 3px;
  width: 100%;
`;

export const Button = styled.button`
  cursor: pointer;
  width: 5rem;
`;
