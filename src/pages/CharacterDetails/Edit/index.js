import React from 'react';
import PropTypes from 'prop-types';

import {
  Container,
  FormGroup,
  Label,
  ContainerInput,
  Input,
  Button,
} from './styles';

export default function Edit({
  character,
  onHandleCollapseFormEdit,
  onHandleNameEdit,
}) {
  return (
    <Container>
      <FormGroup>
        <Label htmlFor="label">Edite o nome do personagem:</Label>
        <ContainerInput>
          <Input
            id="label"
            data-testid="inputName"
            type="text"
            defaultValue={character && character.name ? character.name : ''}
            onFocus={() => {}}
            onBlur={({ target }) => onHandleNameEdit(target.value)}
          />
          <Button
            id="buttonSave"
            data-testid="btnSave"
            onClick={onHandleCollapseFormEdit}
          >
            Salvar
          </Button>
        </ContainerInput>
      </FormGroup>
    </Container>
  );
}

Edit.deafultProps = {
  character: null,
  onHandleCollapseFormEdit: () => {},
  onHandleNameEdit: () => {},
}

Edit.propTypes = {
  character: PropTypes.shape({}),
  onHandleCollapseFormEdit: PropTypes.func,
  onHandleNameEdit: PropTypes.func,
}