import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const MainContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100%;
`;

export const ImageContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 16rem;
  background-color: #212121;
  img {
    margin: 0;
    padding: 0;
    opacity: 0.99;
    transition: opacity 0.5s ease-out;
    -webkit-transition: opacity 1.5s ease-out;
    -moz-transition: opacity 1.5s ease-out;
    -ms-transition: opacity 1.5s ease-out;
    -o-transition: opacity 1.5s ease-out;
  }
`;

export const Name = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: #d3d3d3;
  font-size: 2rem;
  text-transform: uppercase;
  text-align: center;
  padding: 1rem;
  width: 16rem;
  height: 100%;
  margin: 0;
  background-color: #323232;
`
export const ContainerDescription = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1rem;
  max-width: 32rem;
  text-transform: uppercase;
  span {
    font-size: 1rem;
    font-weight: 600;
    text-align:center;
    margin-bottom: 0.8rem;
  }
  p {
    margin: 0.1rem 0;
    font-size: 0.8rem;
  }
`
export const ContainerMenu = styled.div`
  display: flex;
  justify-content: center;
  padding: 0.5rem;
  height: 3rem;
  width: 100%;
  background-color: #212121;
  border-top: 0.02rem solid #585858;
  border-bottom: 0.02rem solid #585858;
`;

export const Menu = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: space-between;
  border-left: 0.02rem solid #585858;
  border-right: 0.02rem solid #585858;
  button {
    background-color: transparent;
    border: none;
    padding: 0.5rem 1rem;
    font-size: 1rem;
    font-weight: bold;
    text-transform: uppercase;
    cursor: pointer;
    color: #d3d3d3;
    &:hover {
      color: #EC1D24;
      transform: scale(1.05);
    }
  }
`;

export const MenuSeparator = styled.div`
  width: 0.03rem;
  margin:0;
  padding:0;
  border-left: 0.03rem solid #585858;
`;

export const HeaderContainer = styled.div`
  display: flex;
  align-items:center;
  justify-content:center;
  width: 100%;
  background-color: #212121;
`;

export const BtnLink = styled(Link)`
  margin: 0;
  padding: 0;
  text-decoration: none;
`;

export const FormEditCollapse = styled.div`
  transition: 0.4s ease-in;
  transition: 0.4s ease-out;
  &.open {
    height: auto;
    padding: 2;
  }
  &.closed {
    height: 0;
    padding: 0;
    overflow: hidden;
  }
`;