import React, { useLayoutEffect, useEffect, useState } from 'react';
import Header from '../../components/Header';
import Loader from '../../components/Loader';
import { useSelector, useDispatch } from 'react-redux'
import {
  fetchCharacterSelectedAction,
  saveCharacterAction
} from '../../redux/characters/actions';
import defaultImage from '../../assets/images/not_avaliable-detail.jpg';
import EditForm from './Edit';

import {
  Container,
  ImageContainer,
  MainContent,
  ContainerDescription,
  Name,
  ContainerMenu,
  Menu,
  HeaderContainer,
  MenuSeparator,
  BtnLink,
  FormEditCollapse,
} from './styles';

export default function CharacterDetail({ match }) {
  const [imageDetail, setImageDetail] = useState(defaultImage);
  const [formEditCollapseOpen, setFormEditCollapseOpen] = useState(false);
  const typeImg = 'standard_fantastic';
  const dispatch = useDispatch();

  const character = useSelector(({ characters }) => ({
    ...characters.selectedCharacter,
    isLoading: characters.isLoading,
  }));

  useEffect(() => {
    function fetchData() {
      try {
        if (match && match.params && match.params.id) {
          dispatch(fetchCharacterSelectedAction(match.params.id));
        }
      } catch (error) {
        window.console.log(error)
      }
    }
    fetchData();
  }, [dispatch, match]);

  useLayoutEffect(() => {
    if (character
      && character.thumbnail
      && character.thumbnail.path
      && character.thumbnail.extension) {
        const path = `${character.thumbnail.path}/${typeImg}.${character.thumbnail.extension}`;
        if (imageDetail !== path) {
          setImageDetail(path);
        }
      }
  }, [character, imageDetail]);

  function onError() {
    setImageDetail(defaultImage)
  }

  function handleNameEdit(value) {
    const newState = {
      id: character.id,
      name: value,
      description: character.description,
      thumbnail: character.thumbnail,
      series: character.series,
    }
    try {
      dispatch(saveCharacterAction(newState));
    } catch (error) {
      window.console.log(error);
    }
  }

  function handleCollapseFormEdit() {
    setFormEditCollapseOpen(!formEditCollapseOpen)
  }

  function renderMain() {
    return (
      <MainContent>
        <ContainerMenu>
          <Menu>
            <BtnLink to="/characters">
              <button id="btnBack" type="button">Voltar</button>
            </BtnLink>
            <MenuSeparator />
            <button
              id="btnEdit"
              type="button"
              onClick={handleCollapseFormEdit}
            >
              Editar
            </button>
          </Menu>
        </ContainerMenu>
        <HeaderContainer>
          <ImageContainer>
            <img
              id="imageCharacterId"
              alt="imageCharacter"
              src={imageDetail}
              onError={onError}
            />
          </ImageContainer>
          <Name>
            <h5>{character.name}</h5>
          </Name>
        </HeaderContainer>
        <FormEditCollapse
          className={formEditCollapseOpen ? 'open' : 'closed'}
        >
          <EditForm
            character={character}
            onHandleCollapseFormEdit={handleCollapseFormEdit}
            onHandleNameEdit={handleNameEdit}
          />
        </FormEditCollapse>
        <ContainerDescription>
          <span>Séries:</span>
          {(character
            && character.series
            && character.series.items
            && character.series.available > 0)
            ? (
              character.series.items.map((serie, index) => (
                <p key={`${index}-${serie.name}`}>{serie.name}</p>
              ))
            ) : (<p>Informações indisponíveis</p>)}
        </ContainerDescription>
      </MainContent>
    );
  }

  return (
    <Container>
      <Header />
      {character.isLoading ? (<Loader/>) : renderMain()}
    </Container>
  );
}
