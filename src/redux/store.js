import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';

import characters from './characters';

const reducers = combineReducers({ characters });

const rootReducer = (state, action) => {
  let newState = { ...state };
  return reducers(newState, action);
};

const composeEnhancers =
  process.env.REACT_APP_ENV !== 'production'
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    : compose;

export default createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));