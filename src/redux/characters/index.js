export const ACTION_TYPE = {
  FETCH_CHARACTERS: 'characters/FETCH_CHARACTERS',
  FETCH_SELECTED_CHARACTER: 'characters/FETCH_BY_ID',
  ISLOADING: 'characters/ISLOADING',
  UNLOADING: 'characters/UNLOADING',
  SEARCH_BY_INITIALS: 'characters/SEARCH_BY_INITIALS',
  SAVE_CHARACTER: 'characters/SAVE_CHARACTER',
}

const INITIAL_STATE = {
  isLoading: false,
  data: [],
  selectedCharacter: null,
  searchByInitials: '',
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ACTION_TYPE.FETCH_CHARACTERS: {
      const { payload } = action;
      return {
        ...state,
        data: payload,
      };
    }
    case ACTION_TYPE.FETCH_SELECTED_CHARACTER: {
      const { payload } = action;
      return {
        ...state,
        selectedCharacter: payload,
      };
    }
    case ACTION_TYPE.SAVE_CHARACTER: {
      const { payload } = action;
      return {
        ...state,
        selectedCharacter: payload,
      };
    }
    case ACTION_TYPE.ISLOADING: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case ACTION_TYPE.UNLOADING: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case ACTION_TYPE.SEARCH_BY_INITIALS: {
      const { payload } = action;
      return {
        ...state,
        searchByInitials: payload,
      };
    }
    default:
      return {
        ...state,
      };
  }
}