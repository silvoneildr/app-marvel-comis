import { ACTION_TYPE } from './index';
import {
  fetchCharacters,
  fetchCharacterById,
} from '../../services/charactersService';

export const fetchCharactersAction = (params = '') => async dispatch => {
  try {
    dispatch({ type: ACTION_TYPE.ISLOADING });
    const { data } = await fetchCharacters(params);
    dispatch({
      type: ACTION_TYPE.FETCH_CHARACTERS,
      payload: data && data.results && data.results.length
        ? [...data.results.map(item => ({
          id: item.id,
          name: item.name,
          thumbnail: item.thumbnail,
        }))]
        : [],
    });
    return data;
  } catch (err) {
    throw err.response;
  } finally {
    dispatch({ type: ACTION_TYPE.UNLOADING });
  }
};

export const fetchCharacterSelectedAction = id => async dispatch => {
  try {
    dispatch({ type: ACTION_TYPE.ISLOADING });
    const { data } = await fetchCharacterById(id);
    dispatch({
      type: ACTION_TYPE.FETCH_SELECTED_CHARACTER,
      payload: data && data.results && data.results.length
        ?
          {
            id: data.results[0].id,
            name: data.results[0].name,
            description: data.results[0].description,
            thumbnail: data.results[0].thumbnail,
            series: data.results[0].series,
          }
        : null,
    });
    return data;
  } catch (err) {
    throw err.response;
  } finally {
    dispatch({ type: ACTION_TYPE.UNLOADING });
  }
};

export const searchByInitials = payload => ({
  type: ACTION_TYPE.SEARCH_BY_INITIALS,
  payload,
});

export const saveCharacterAction = payload => ({
  type: ACTION_TYPE.SAVE_CHARACTER,
  payload,
});