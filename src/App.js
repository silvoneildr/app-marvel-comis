import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import GlobalStyles from './assets/styles/global';

import Routes from './routes';
import store from './redux/store';

export default () => (
  <Provider store={store}>
    <BrowserRouter basename="/characters">>
      <GlobalStyles />
      <Routes />
    </BrowserRouter>
  </Provider>
);
