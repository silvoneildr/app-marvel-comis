const baseUrl = 'https://gateway.marvel.com/v1/public';
const ts = 'ts=1586472140';
const apikey = 'apikey=3fc4df6250fdabd3afbbbffcc29f3602';
const hash = 'hash=9d27e9f019a4c22de139f1596ad79f33';
const limit = 'limit=30';

export async function fetchCharacters(params) {
  let newParams = `?${ts}&${apikey}&${hash}&${limit}`;
  if (params && params !== '') {
    newParams = newParams + `&nameStartsWith=${params.toString()}`
  }
  const options = {
    method: 'GET',
    node: 'cors',
    cache: 'default',
  }
  let data;
  try {
    data = fetch(`${baseUrl}/characters${newParams}`, options)
    .then(resp => {
      if (resp.status === 200) {
        return resp.json();
      }
    })
    .catch(error => window.console.log(error))
  } catch (error) {
    window.console.log(error)
  }
  return data;
}

export async function fetchCharacterById(id) {
  const params = `${id}?${ts}&${apikey}&${hash}`;
  const options = {
    method: 'GET',
    node: 'cors',
    cache: 'default',
  }
  let data;
  try {
    data = fetch(`${baseUrl}/characters/${params}`, options)
    .then(resp => {
      if (resp.status === 200) {
        return resp.json();
      }
    })
    .catch(error => window.console.log(error))
  } catch (error) {
    window.console.log(error)
  }
  return data;
}

export async function fetchImageByPath(path) {
  const options = {
    method: 'GET',
    node: 'cors',
    cache: 'default',
  }
  let image;
  try {
    image = fetch(`${path}`, options)
    .then(resp => {
      if (resp.status === 200) {
        return resp;
      }
    })
    .catch(error => window.console.log(error))
  } catch (error) {
    window.console.log(error)
  }
  return image;
}
