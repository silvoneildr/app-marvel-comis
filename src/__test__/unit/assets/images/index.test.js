import React from 'react';
import { render } from '@testing-library/react';
import LogoMarvel from '../../../../assets/images/LogoMarvel'

describe("Render LogoMarvel component", () => {
  test('should render the LogoMarvel component', () => {
    const wrapper = render(<LogoMarvel/>);
    expect(wrapper).toMatchSnapshot();
  });
});