import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Edit from '../../../../../pages/CharacterDetails/Edit';

describe("Render Edit component", () => {
  const onHandleCollapseFormEdit = jest.fn();
  const onHandleNameEdit = jest.fn();
  const  character = {
    id: 1011334,
    name: '3-D Man',
    description: '',
    thumbnail: {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784',
      extension: 'jpg'
    },
    series: {
      available: 3,
      collectionURI: 'http://gateway.marvel.com/v1/public/characters/1011334/series',
      items: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/1945',
          name: 'Avengers: The Initiative (2007 - 2010)'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/2005',
          name: 'Deadpool (1997 - 2002)'
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/2045',
          name: 'Marvel Premiere (1972 - 1981)'
        }
      ],
      returned: 3
    }
  };

  test('should render the Edit component', () => {
    const { container } = render(
      <Edit
        character={character}
        onHandleCollapseFormEdit={onHandleCollapseFormEdit}
        onHandleNameEdit={onHandleNameEdit}
      />
    );
    expect(container).toMatchSnapshot();
  });

  test('should handle Edit button click', async () => {
    const { getByText } = render(
      <Edit
        character={character}
        onHandleCollapseFormEdit={onHandleCollapseFormEdit}
        onHandleNameEdit={onHandleNameEdit}
      />
    );
    const buttonSave = screen.getByTestId('btnSave');

    fireEvent.click(buttonSave);
    expect(getByText('Salvar'));
  });
});