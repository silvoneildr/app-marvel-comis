import React from 'react';
import { Provider } from 'react-redux';
import { render, fireEvent } from '@testing-library/react';
import { createStore } from 'redux'
import CharacterDetails from '../../../../pages/CharacterDetails';
import { BrowserRouter } from 'react-router-dom';

describe("Render CharacterDetails component", () => {
  const store = createStore(() => ({
    characters: {
      isLoading: false,
      data: [
        {
          id: 1011334,
          name: '3-D Man',
          thumbnail: {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784',
            extension: 'jpg'
          }
        },
        {
          id: 1017100,
          name: 'A-Bomb (HAS)',
          thumbnail: {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16',
            extension: 'jpg'
          }
        },
      ],
      selectedCharacter: {
        id: 1011334,
        name: '3-D Man',
        description: '',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784',
          extension: 'jpg'
        },
        series: {
          available: 3,
          collectionURI: 'http://gateway.marvel.com/v1/public/characters/1011334/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1945',
              name: 'Avengers: The Initiative (2007 - 2010)'
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2005',
              name: 'Deadpool (1997 - 2002)'
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2045',
              name: 'Marvel Premiere (1972 - 1981)'
            }
          ],
          returned: 3
        }
      },
    }
  }));
  test('should render the CharacterDetails component', () => {
    const wrapper = render(
      <Provider store={store}>
        <BrowserRouter>
          <CharacterDetails/>
        </BrowserRouter>
      </Provider>,
    );
    expect(wrapper).toMatchSnapshot();
  });
});