import React from 'react';
import { Provider } from 'react-redux';
import { render, fireEvent } from '@testing-library/react';
import { createStore } from 'redux'
import Characters from '../../../../pages/Characters';
import { BrowserRouter } from 'react-router-dom';

describe("Render Characters component", () => {
  const store = createStore(() => ({
    characters: {
      isLoading: false,
      data: [
        {
          id: 1011334,
          name: '3-D Man',
          thumbnail: {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784',
            extension: 'jpg'
          }
        },
        {
          id: 1017100,
          name: 'A-Bomb (HAS)',
          thumbnail: {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16',
            extension: 'jpg'
          }
        },
      ],
    }
  }));

  test('should render the Characters component', () => {
    const wrapper = render(
      <Provider store={store}>
        <BrowserRouter>
          <Characters/>
        </BrowserRouter>
      </Provider>,
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('should render input type text', async () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <BrowserRouter>
          <Characters/>
        </BrowserRouter>
      </Provider>,
    );
    const input = getByTestId('inputSearch');
    const inputText = 'Input tests';

    fireEvent.change(input , { target: { value: inputText } });
    fireEvent.keyPress(input, { key: "Enter", code: 13 });

    expect(input).toBeVisible();
    expect(input).toHaveAttribute('type', 'text');
    expect(input.value).toEqual(inputText);
  });
});
