import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Card from '../../../../components/Card';

describe("Render Card component", () => {
  test('should render the Card component', () => {
    const wrapper = render(<Card name="nameTestCard" imagePath="pathImgTestCard"/>);
    expect(wrapper).toMatchSnapshot();
  });
});