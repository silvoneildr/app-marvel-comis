import React from 'react';
import { render } from '@testing-library/react';
import Loader from '../../../../components/Loader';

describe("Render Loader component", () => {
  test('should render the Loader component', () => {
    const wrapper = render(<Loader/>);
    expect(wrapper).toMatchSnapshot();
  });
});