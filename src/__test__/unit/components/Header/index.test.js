import React from 'react';
import { render } from '@testing-library/react';
import Header from '../../../../components/Header';

describe("Render Header component", () => {
  test('should render the Header component', () => {
    const wrapper = render(<Header/>);
    expect(wrapper).toMatchSnapshot();
  });
});