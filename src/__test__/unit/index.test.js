import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { renderToDOM } from '../../index';
import { createStore } from 'redux'
import App from '../../App';

const store = createStore(() => ({
  characters: {
    isLoading: false,
    data: [
      {
        id: 1011334,
        name: '3-D Man',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784',
          extension: 'jpg'
        }
      },
      {
        id: 1017100,
        name: 'A-Bomb (HAS)',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16',
          extension: 'jpg'
        }
      },
    ],
  }
}));

describe("Call React.DOM root element...", () => {
  test('should call renderToDOM function with a null root element', () => {
    renderToDOM(App, null);
  });
  test('should call renderToDOM function with a valid root element', () => {
    const div = document.createElement('div');
    renderToDOM(App, div);
  });
});

describe("Render App component", () => {
  test('should render the App component', () => {
    const wrapper = render(
      <Provider store={store}>
        <App />
      </Provider>,
    );
    expect(wrapper).toMatchSnapshot();
  });
});