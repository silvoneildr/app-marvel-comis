import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

function renderToDOM(component, rootElement) {
  if (rootElement) {
    ReactDOM.render(component, rootElement);
  }
}

renderToDOM(<App />, document.getElementById('root'));

export { renderToDOM };
