import { createGlobalStyle } from 'styled-components';
import OpenSansRegular from '../fonts/OpenSansRegular.ttf';

export default createGlobalStyle`
  @font-face {
    font-family: 'Open Sans';
    src: url(${OpenSansRegular});
  }
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }
  html, body, #root {
    height: 100%;
    background-color: #FAFAFA;
  }
  body {
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing: antialiased !important;
    font-family: Open Sans;
    color: #707070;
  }
`;
